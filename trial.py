import contextlib
import heapq
import os
import pickle
import string
import time

from nltk.tokenize import word_tokenize
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import \
    StopWordRemoverFactory
from tqdm import tqdm

from compression import StandardPostings, VBEPostings
from index import InvertedIndexReader, InvertedIndexWriter
from util import IdMap, sorted_intersect


stemmer = StemmerFactory().create_stemmer()
stopword = StopWordRemoverFactory().create_stop_word_remover()
translator = str.maketrans(f"{string.punctuation}{string.digits}",
                           ' '*len(f"{string.punctuation}{string.digits}"))

doc_id_map = IdMap()
term_id_map = IdMap()
data_dir = 'collection'
block_dir_relative = '0'

results = []
# diberikan list of block(folder) inside collection folder
base_document_path = f"{data_dir}/{block_dir_relative}"
# iterate docs
for doc_name in tqdm(sorted(next(os.walk(base_document_path))[2]), leave=None):
    # get doc id
    doc_id = doc_id_map[doc_name]
    # read doc and preprocess
    doc_path = f"{base_document_path}/{doc_name}"
    f = open(doc_path, "r")
    content = f.read()
    text = stemmer.stem(content)
    text = stopword.remove(text)
    text = text.translate(translator)
    doc = word_tokenize(text)
    # generate term-doc pair
    for term in tqdm(doc, leave=False):
        term_id = term_id_map[term]
        results.append((term_id, doc_id))


td_pairs = results

term_dict = {}
for term_id, doc_id in td_pairs:
    if term_id not in term_dict:
        term_dict[term_id] = set()
    term_dict[term_id].add(doc_id)
